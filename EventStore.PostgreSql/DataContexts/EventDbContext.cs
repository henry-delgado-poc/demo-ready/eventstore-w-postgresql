﻿using Microsoft.EntityFrameworkCore;
using EventStore.PostgreSql.EventModels;
using EventStore.PostgreSql.DomainModels;

namespace EventStore.PostgreSql.DataContexts
{
    public class EventDbContext: DbContext
    {
        public EventDbContext(DbContextOptions<EventDbContext> options) : base(options) { }

        public DbSet<Event<Company>> CompanyEvents { get; set; }
        public DbSet<Event<Project>> ProjectEvents { get; set; }
        public DbSet<Event<Penetration>> PenetrationEvents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("demo");

            modelBuilder.Entity<Event<Company>>()
               .HasIndex(c => c.AggregateId);

            modelBuilder.Entity<Event<Project>>()
               .HasIndex(c => c.AggregateId);

            modelBuilder.Entity<Event<Penetration>>()
               .HasIndex(c => c.AggregateId);

            modelBuilder.Entity<Event<Company>>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
        }

    }
}
