﻿using Microsoft.EntityFrameworkCore;
using EventStore.PostgreSql.DomainModels;

namespace EventStore.PostgreSql.DataContexts
{
    public class DomainDbContext: DbContext
    {
        public DomainDbContext(DbContextOptions<DomainDbContext> options) : base(options) { }
        
        public DbSet<Company> Companies { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Penetration> Penetrations { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("demo");

            modelBuilder.Entity<Company>()
                .HasIndex(c => c.Name);
            modelBuilder.Entity<Project>()
                .HasIndex(p => p.Name);            
            
            modelBuilder.Entity<Project>()
                .HasOne(x => x.Company)
                .WithMany(x => x.Projects)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Penetration>()
                .HasOne(p => p.Project)
                .WithMany(x => x.Penetrations)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Company>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Project>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Penetration>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
        }
    }
}
