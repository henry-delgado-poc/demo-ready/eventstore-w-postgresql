﻿using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.DomainStores;
using EventStore.PostgreSql.EventModels;
using EventStore.PostgreSql.EventStores;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.Controllers
{
    /// <summary>
    /// Project Penetrations functionality
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PenetrationController : ControllerBase
    {
        private readonly IPenetrationDomainStore _domainStore;
        private readonly IEventStore<Penetration> _eventStore;

        public PenetrationController(IPenetrationDomainStore domainStore, IEventStore<Penetration> eventStore)
        {
            _domainStore = domainStore;
            _eventStore = eventStore;
        }

        /// <summary>
        /// Gets all penetrations for the given project
        /// </summary>
        /// <param name="projectId">(long) Project ID</param>
        /// <returns>List of project penetrations</returns>
        [HttpGet]
        public ActionResult<List<Penetration>> GetAllPenetrationsByProject(long projectId)
        {
            try
            {
                var result = _domainStore.GetAllPenetrationsByProjectId(projectId);
                if (result != null && result.Any())
                    return Ok(result);
                return NoContent();
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine(ae);
                return BadRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Get Project Penetrations
        /// </summary>
        /// <returns>The project</returns>
        [HttpGet("{id:long}")]
        public ActionResult<Penetration> Get(long id)
        {
            try
            {
                return Ok(_domainStore.GetPenetrationById(id));
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine(ae);
                return BadRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Create new Project Penetration
        /// </summary>
        /// <param name="penetration">Penetration to create</param>
        /// <returns>The new Penetration created including its new id</returns>
        [HttpPost]
        public async Task<ActionResult<Event<Penetration>>> Post(Penetration penetration)
        {
            //skipping validations for demo
            try
            {
                var newPenetration = await _domainStore.CreatePenetration(penetration);
                var newEvent = await _eventStore.RaiseEvent(new Event<Penetration>(newPenetration.Id, DateTime.UtcNow, "addedPenetration", newPenetration));

                return Ok(newEvent);

            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Updates an existing penetration
        /// </summary>
        /// <param name="penetration">The penetration values to update</param>
        /// <returns>The updated penetration</returns>
        [HttpPut]
        public async Task<ActionResult<Event<Penetration>>> Put(Penetration penetration)
        {
            //skipping validations for demo
            try
            {
                var updatedPenetration = await _domainStore.UpdatePenetration(penetration);
                var newEvent = await _eventStore.RaiseEvent(new Event<Penetration>(updatedPenetration.Id, DateTime.UtcNow, "updatedPenetration", updatedPenetration));

                return Ok(newEvent);
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes an existing penetration
        /// </summary>
        /// <param name="id">Penetration ID to delete</param>        
        [HttpDelete]
        public async Task<ActionResult<Event<Penetration>>> Delete(long id)
        {
            if (id < 1)
                return BadRequest();

            try
            {
                var penetration = _domainStore.GetPenetrationById(id);
                await _domainStore.DeletePenetration(id);
                var newEvent = await _eventStore.RaiseEvent(new Event<Penetration>(id, DateTime.UtcNow, "deletedPenetration", penetration));

                return Ok(newEvent);
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Get penetrations ledger
        /// </summary>
        /// <param name="aggreateId">Penetration Id</param>
        /// <param name="firstSequence">(optional) Initial point in ledger</param>
        /// <param name="lastSequence">(optional) Final point in ledger</param>
        /// <returns></returns>
        [Route("GetPenetrationEvents")]
        [HttpGet]
        public ActionResult<Event<Penetration>> GetPenetrationEvents(long aggreateId, long firstSequence, long lastSequence)
        {
            if (aggreateId < 1)
                return BadRequest($"Missing {nameof(aggreateId)}");

            var events = _eventStore.GetEvents(aggreateId, firstSequence, lastSequence);
            if (events == null || !events.Any())
                return NoContent();
            return Ok(events);
        }
    }
}
