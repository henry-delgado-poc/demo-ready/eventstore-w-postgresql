﻿using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.DomainStores;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.PostgreSql.EventStores;
using EventStore.PostgreSql.EventModels;
using System.Linq;

namespace EventStore.PostgreSql.Controllers
{
    /// <summary>
    /// Company functionality
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyDomainStore _domainStore;
        private readonly IEventStore<Company> _eventStore;

        public CompanyController(ICompanyDomainStore domainStore, IEventStore<Company> eventStore)
        {
            _domainStore = domainStore;
            _eventStore = eventStore;
        }

        /// <summary>
        /// Gets all available companies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<Company>> Get()
        {
            try
            {
                var result = _domainStore.GetCompanies();
                if (result != null && result.Any())
                    return Ok(result);
                return NoContent();
            }
            catch(Exception ex)
            {
                //for demo only. excluding logs and error handlers
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets Company
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        public ActionResult<Company> Get(long id)
        {
            try
            {
                var data = _domainStore.GetCompany(id);
                return data !=null ? Ok(data) : NoContent();
            }
            catch (ArgumentNullException e)
            {
                //for demo only. excluding logs and error handlers
                Console.WriteLine(e);
                return BadRequest();
            }
            catch (Exception ex)
            {
                //for demo only. excluding logs and error handlers
                Console.WriteLine(ex);
                return NotFound();
            }
        }

        /// <summary>
        /// Create new Company
        /// </summary>
        /// <param name="company">Company to create</param>
        /// <returns>The new company created including its new id</returns>
        [HttpPost]
        public async Task<ActionResult<Event<Company>>> Post(Company company)
        {
            //skipping validations for demo
            try
            {
                var newCompany = await _domainStore.CreateCompany(company);
                var newEvent= await _eventStore.RaiseEvent(new Event<Company>(newCompany.Id, DateTime.UtcNow, "addedCompany", newCompany));

                return Ok(newEvent);

            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Updates an existing company
        /// </summary>
        /// <param name="company">The company values to update</param>
        /// <returns>The updated company</returns>
        [HttpPut]
        public async Task<ActionResult<Event<Company>>> Put(Company company)
        {
            //skipping validations for demo
            try
            {
                var updatedCompany = await _domainStore.UpdateCompany(company);
                var newEvent = await _eventStore.RaiseEvent(new Event<Company>(updatedCompany.Id, DateTime.UtcNow, "updatedCompany", updatedCompany));

                return Ok(newEvent);
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes an existing company
        /// </summary>
        /// <param name="id">Company ID to delete</param>        
        [HttpDelete]
        public async Task<ActionResult<Event<Company>>> Delete(long id)
        {
            if (id < 1)
                return BadRequest();

            try
            {
                var company = _domainStore.GetCompany(id);
                await _domainStore.DeleteCompany(id);
                var newEvent = await _eventStore.RaiseEvent(new Event<Company>(id, DateTime.UtcNow, "deletedCompany", company));

                return Ok(newEvent);                
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Get company ledger
        /// </summary>
        /// <param name="aggreateId">Company Id</param>
        /// <param name="firstSequence">(optional) Initial point in ledger</param>
        /// <param name="lastSequence">(optional) Final point in ledger</param>
        /// <returns></returns>
        [Route("GetCompanyEvents")]
        [HttpGet]
        public ActionResult<Event<Company>> GetCompanyEvents(long aggreateId, long firstSequence, long lastSequence)
        {
            if (aggreateId < 1)
                return BadRequest($"Missing {nameof(aggreateId)}");

            var events = _eventStore.GetEvents(aggreateId, firstSequence, lastSequence);
            if (events == null || !events.Any())
                return NoContent();
            return Ok(events);
        }
    }
}
