﻿using System.Threading.Tasks;
using EventStore.PostgreSql.DomainStores;
using Microsoft.AspNetCore.Mvc;


namespace EventStore.PostgreSql.Controllers
{
    /// <summary>
    /// Dummy data functionality
    /// </summary>
    [Route("api/[controller]")]
    public class SeederController : Controller
    {
        private readonly ISeeder _service;

        public SeederController(ISeeder service)
        {
            _service = service;
        }

        
        /// <summary>
        /// Remove existing data and generates dummy records as per given parameters
        /// </summary>
        /// <param name="totalCompanies"></param>
        /// <param name="totalProjectsPerCompany"></param>
        /// <param name="totalPenetrationsPerProject"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(int totalCompanies=2, int totalProjectsPerCompany=10, int totalPenetrationsPerProject=10, int totalUpdatesPerRecord = 4)
        {
            await _service.GenerateCleanDemoData(totalCompanies, totalProjectsPerCompany, totalPenetrationsPerProject, totalUpdatesPerRecord);
            return Ok();
        }
    }
}
