﻿using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.DomainStores;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.PostgreSql.EventStores;
using EventStore.PostgreSql.EventModels;
using System.Linq;

namespace EventStore.PostgreSql.Controllers
{
    /// <summary>
    /// Projects functionality
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectDomainStore _domainStore;
        private readonly IEventStore<Project> _eventStore;

        public ProjectController(IProjectDomainStore domainStore, IEventStore<Project> eventStore)
        {
            _domainStore = domainStore;
            _eventStore = eventStore;
        }

        /// <summary>
        /// Gets all projects for the given company
        /// </summary>
        /// <param name="companyId">(long) Company ID</param>
        /// <returns>List of projects</returns>
        [HttpGet]
        public ActionResult<List<Project>> GetAllProjectsByCompany(long companyId)
        {
            try
            {
                var result = _domainStore.GetAllProjectsByCompanyId(companyId);
                if(result !=null && result.Any())
                    return Ok(result);
                return NoContent();
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine(ae);
                return BadRequest();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Gets Project
        /// </summary>
        /// <returns>The project</returns>
        [HttpGet("{id:long}")]
        public ActionResult<Project> Get(long id)
        {
            try
            {
                return Ok(_domainStore.GetProjectById(id));
            }
            catch (ArgumentNullException ae)
            {
                Console.WriteLine(ae);
                return BadRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Create new Project
        /// </summary>
        /// <param name="project">Project to create</param>
        /// <returns>The new project created including its new id</returns>
        [HttpPost]
        public async Task<ActionResult<Event<Project>>> Post(Project project)
        {
            //skipping validations for demo
            try
            {
                var newProject = await _domainStore.CreateProject(project);
                var newEvent = await _eventStore.RaiseEvent(new Event<Project>(newProject.Id, DateTime.UtcNow, "addedProject", newProject));

                return Ok(newEvent);

            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Updates an existing project
        /// </summary>
        /// <param name="project">The project values to update</param>
        /// <returns>The updated project</returns>
        [HttpPut]
        public async Task<ActionResult<Event<Project>>> Put(Project project)
        {
            //skipping validations for demo
            try
            {
                var updatedProject = await _domainStore.UpdateProject(project);
                var newEvent = await _eventStore.RaiseEvent(new Event<Project>(updatedProject.Id, DateTime.UtcNow, "updatedProject", updatedProject));

                return Ok(newEvent);
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes an existing project
        /// </summary>
        /// <param name="id">Project ID to delete</param>        
        [HttpDelete]
        public async Task<ActionResult<Event<Project>>> Delete(long id)
        {
            if (id < 1)
                return BadRequest();

            try
            {
                var project = _domainStore.GetProjectById(id);
                await _domainStore.DeleteProject(id);
                var newEvent = await _eventStore.RaiseEvent(new Event<Project>(id, DateTime.UtcNow, "deletedProject", project));

                return Ok(newEvent);
            }
            catch (ArgumentException ex)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(ex);
                return BadRequest();
            }
            catch (Exception e)
            {
                //for demo, skipping logs and proper handling
                Console.WriteLine(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Get project ledger
        /// </summary>
        /// <param name="aggreateId">Project Id</param>
        /// <param name="firstSequence">(optional) Initial point in ledger</param>
        /// <param name="lastSequence">(optional) Final point in ledger</param>
        /// <returns></returns>
        [Route("GetProjectEvents")]
        [HttpGet]
        public ActionResult<Event<Company>> GetProjectEvents(long aggreateId, long firstSequence, long lastSequence)
        {
            if (aggreateId < 1)
                return BadRequest($"Missing {nameof(aggreateId)}");

            var events = _eventStore.GetEvents(aggreateId, firstSequence, lastSequence);
            if (events == null || !events.Any())
                return NoContent();
            return Ok(events);
        }
    }
}
