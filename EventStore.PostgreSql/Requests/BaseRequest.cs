﻿namespace EventStore.PostgreSql.Requests
{
    public class BaseRequest
    {
        /// <summary>
        /// Session Identifier
        /// </summary>
        public string CorrelationId { get; set; }
    }
}
