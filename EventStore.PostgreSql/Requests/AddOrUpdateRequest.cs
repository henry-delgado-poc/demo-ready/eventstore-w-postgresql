﻿namespace EventStore.PostgreSql.Requests
{
    /// <summary>
    /// Request to add or update the given entity
    /// </summary>
    /// <typeparam name="T">A generic object representing the data to be added or updated</typeparam>
    public class AddOrUpdateRequest<T> : BaseRequest
    {
        /// <summary>
        /// Entity to be added or updated
        /// </summary>
        public T Data { get; set; }
    }
}
