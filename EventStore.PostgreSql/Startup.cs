using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.DomainStores;
using EventStore.PostgreSql.EventStores;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace EventStore.PostgreSql
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<DomainDbContext>(d => d.UseNpgsql(Configuration.GetConnectionString("DomainDbConnection")));
            services.AddDbContext<EventDbContext>(d => d.UseNpgsql(Configuration.GetConnectionString("EventStoreConnection")));

            services.AddTransient<ICompanyDomainStore, CompanyDomainStore>();
            services.AddTransient<IProjectDomainStore, ProjectDomainStore>();
            services.AddTransient<IPenetrationDomainStore, PenetrationDomainStore>();
            services.AddTransient<IEventStore<Company>, CompanyEventStore>();
            services.AddTransient<IEventStore<Project>, ProjectEventStore>();
            services.AddTransient<IEventStore<Penetration>, PenetrationEventStore>();
            services.AddTransient<ISeeder, Seeder>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Event Sourcing with PostgreSql as Event Store", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "EventStore.PostgreSql v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
