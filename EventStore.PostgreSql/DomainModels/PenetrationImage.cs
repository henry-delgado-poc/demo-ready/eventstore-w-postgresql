﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EventStore.PostgreSql.DomainModels
{
    /// <summary>
    /// Image related to a project floor penetration
    /// </summary>
    public class PenetrationImage
    {
        /// <summary>
        /// Image ID
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Was image taken before the installation?
        /// <para>true = Pre-Install image</para>
        /// <para>false = Post-Install image</para>
        /// </summary>
        public bool IsPreInstall { get; set; }

        /// <summary>
        /// The URL where the image is stored
        /// </summary>
        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        /// <summary>
        /// The penetration ID related to the image
        /// </summary>
        [Required]
        public long PenetrationId { get; set; }

        /// <summary>
        /// Penetration
        /// </summary>
        [ForeignKey("PenetrationId")]
        [JsonIgnore]
        public virtual Penetration Penetration { get; set; }

        /// <summary>
        /// Image related to a project floor penetration
        /// </summary>
        /// <param name="id">Image ID</param>
        /// <param name="isPreInstall">Indicates whether the image was taken on pre-installation</param>
        /// <param name="imageUrl">Image URL</param>
        /// <param name="penetrationId">Penetation related to image</param>
        public PenetrationImage(long id, bool isPreInstall, string imageUrl, long penetrationId)
        {
            Id = id;
            IsPreInstall = isPreInstall;
            ImageUrl = imageUrl;
            PenetrationId = penetrationId;
        }

        /// <summary>
        /// Image related to a project floor penetration
        /// </summary>
        /// <param name="isPreInstall">Indicates whether the image was taken on pre-installation</param>
        /// <param name="imageUrl">Image URL</param>
        /// <param name="penetrationId">Penetation related to image</param>
        public PenetrationImage(bool isPreInstall, string imageUrl, long penetrationId)
        {
            IsPreInstall = isPreInstall;
            ImageUrl = imageUrl;
            PenetrationId = penetrationId;
        }
    }

}
