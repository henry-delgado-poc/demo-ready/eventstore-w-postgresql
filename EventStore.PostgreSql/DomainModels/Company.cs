﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EventStore.PostgreSql.DomainModels
{
    /// <summary>
    /// Company
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Company Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Company Name
        /// </summary>
        [MaxLength(300)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Company Projects
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Project> Projects { get; set; } = new List<Project>();

        /// <summary>
        /// Company
        /// </summary>
        /// <param name="id">Company ID</param>
        /// <param name="name">Company Name</param>
        public Company(long id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Company
        /// </summary>
        /// <param name="name">Company Name</param>
        public Company(string name)
        {        
            Name = name;
        }

        /// <summary>
        /// Company
        /// </summary>
        public Company()
        {

        }
    }
}
