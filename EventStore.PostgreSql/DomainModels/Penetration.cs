﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EventStore.PostgreSql.DomainModels
{
    /// <summary>
    /// Project Floor Penetration 
    /// </summary>
    public class Penetration
    {
        /// <summary>
        /// Penetration ID
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Project where the penetration is performed
        /// </summary>
        [Required]
        public long ProjectId { get; set; }

        /// <summary>
        /// Project
        /// </summary>
        [JsonIgnore]
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }
        
        /// <summary>
        /// Product ID. For demo only. (might have more in real life)
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// UL-System
        /// </summary>
        public long UlSystem { get; set; }

        /// <summary>
        /// The floor name where penetration is performed
        /// </summary>
        [MaxLength(100)]
        public string Floor { get; set; }

        /// <summary>
        /// The person performing penetration
        /// </summary>
        [MaxLength(300)]
        public string Installer { get; set; }

        /// <summary>
        /// Comments 
        /// </summary>
        [MaxLength(500)]
        public string Comments { get; set; }

        /// <summary>
        /// Penetration Date
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        /// <summary>
        /// List of image URLs fpr the penetration
        /// </summary>
        [JsonIgnore]
        public ICollection<PenetrationImage> PenetrationImages { get; set; } = new List<PenetrationImage>();

        /// <summary>
        /// Project Floor Penetration
        /// </summary>
        /// <param name="id">Penetration ID</param>
        /// <param name="projectId">Project ID</param>
        /// <param name="productId">Product ID</param>
        /// <param name="ulSystem">UL-System used</param>
        /// <param name="floor">Floor Name or Floor Number</param>
        /// <param name="installer">Person performing penetration</param>
        /// <param name="comments">Comments</param>
        /// <param name="date">Date when penetration was completed</param>
        public Penetration(long id, long projectId, long productId, long ulSystem, string floor, string installer, string comments, DateTime date)
        {
            Id = id;
            ProjectId = projectId;
            ProductId = productId;
            UlSystem = ulSystem;
            Floor = floor;
            Installer = installer;
            Comments = comments;
            Date = date;
        }

        /// <summary>
        /// Project Floor Penetration
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="projectId">Project ID</param>
        /// <param name="ulSystem">UL-System used</param>
        /// <param name="floor">Floor Name or Floor Number</param>
        /// <param name="installer">Person performing penetration</param>
        /// <param name="comments">Comments</param>
        /// <param name="date">Date when penetration was completed</param>
        public Penetration(long projectId, long productId, long ulSystem, string floor, string installer, string comments, DateTime date)
        {
            ProjectId = projectId;
            ProductId = productId;
            UlSystem = ulSystem;
            Floor = floor;
            Installer = installer;
            Comments = comments;
            Date = date;
        }

        public Penetration()
        {

        }
    }

}