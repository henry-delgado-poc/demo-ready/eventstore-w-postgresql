﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EventStore.PostgreSql.DomainModels
{
    /// <summary>
    /// Company Project
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Company Project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <param name="companyId">Company owning the projects</param>
        /// <param name="name">Project Name</param>
        /// <param name="building">Building Name</param>
        /// <param name="subContractorName"></param>
        public Project(long id, long companyId, string name, string building, string subContractorName)
        {
            this.Id = id;
            this.Name = name;
            this.Building = building;
            this.SubContractorName = subContractorName;
            this.CompanyId = companyId;
        }

        /// <summary>
        /// Company Project
        /// </summary>
        /// <param name="companyId">Company owning the projects</param>
        /// <param name="name">Project Name</param>
        /// <param name="building">Building Name</param>
        /// <param name="subContractorName"></param>
        public Project(long companyId, string name, string building, string subContractorName)
        {
            this.Name = name;
            this.Building = building;
            this.SubContractorName = subContractorName;
            this.CompanyId = companyId;
        }

        /// <summary>
        /// Company Project
        /// </summary>
        public Project()
        {

        }

        /// <summary>
        /// Project ID
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Project Name
        /// </summary>
        [MaxLength(300)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Building Name
        /// </summary>
        [MaxLength(300)]
        public string Building { get; set; }

        /// <summary>
        /// Sub Contractor Name
        /// </summary>
        [MaxLength(300)]
        public string SubContractorName { get; set; }

        /// <summary>
        /// Company owning the projects
        /// </summary>
        [Required]
        public long CompanyId { get; set; }

        /// <summary>
        /// Company owning the project
        /// </summary>
        [JsonIgnore]
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        /// <summary>
        /// Project penetrations
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Penetration> Penetrations { get; set; } = new List<Penetration>();
    }

}
