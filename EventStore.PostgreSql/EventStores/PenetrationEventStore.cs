﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.EventModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.EventStores
{
    /// <summary>
    /// Project Penetrations
    /// </summary>
    public class PenetrationEventStore: IEventStore<Penetration>
    {
        private readonly EventDbContext _context;

        public PenetrationEventStore(EventDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Event<Penetration>> GetEvents(long aggregateId, long? firstSequenceId = null, long? lastEventSequenceNumber = null)
        {
            if (aggregateId < -1)
                return null;

            var query = _context.PenetrationEvents.Where(x => x.AggregateId == aggregateId);
            if (firstSequenceId != null && firstSequenceId > 0)
            {
                query = query.Where(x => x.Id >= firstSequenceId);
            }
            if (lastEventSequenceNumber != null && lastEventSequenceNumber > 0)
            {
                query = query.Where(x => x.Id <= lastEventSequenceNumber);
            }
            return query.ToList();
        }

        public async Task<Event<Penetration>> RaiseEvent(Event<Penetration> @event)
        {
            await _context.PenetrationEvents.AddAsync(@event);
            await _context.SaveChangesAsync();
            return @event;
        }
    }
}
