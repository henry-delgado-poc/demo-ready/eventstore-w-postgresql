﻿using EventStore.PostgreSql.EventModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.EventStores
{
    public interface IEventStore<T>
    {
        Task<Event<T>> RaiseEvent(Event<T> @event);
        IEnumerable<Event<T>> GetEvents(long aggregateId, long? firstSequenceId = null, long? lastEventSequenceNumber = null);
    }
}
