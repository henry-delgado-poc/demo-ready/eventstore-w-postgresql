﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.EventModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.EventStores
{
    public class CompanyEventStore : IEventStore<Company>
    {
        private readonly EventDbContext _context;
        public CompanyEventStore(EventDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Event<Company>> GetEvents(long aggregateId, long? firstSequenceId= null, long? lastEventSequenceNumber = null)
        {
            if (aggregateId < -1)
                return null;

            var query = _context.CompanyEvents.Where(x => x.AggregateId == aggregateId);
            if(firstSequenceId != null && firstSequenceId > 0)
            {
                query = query.Where(x => x.Id >= firstSequenceId);
            }
            if(lastEventSequenceNumber != null && lastEventSequenceNumber > 0)
            {
                query = query.Where(x => x.Id <= lastEventSequenceNumber);
            }
            return query.ToList();
        }

        public async Task<Event<Company>> RaiseEvent(Event<Company> @event)
        {
            await _context.CompanyEvents.AddAsync(@event);
            await _context.SaveChangesAsync();
            return @event;
        }
    }
}
