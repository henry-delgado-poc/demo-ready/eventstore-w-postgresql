﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.EventModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.EventStores
{
    public class ProjectEventStore : IEventStore<Project>
    {
        private readonly EventDbContext _context;
        public ProjectEventStore(EventDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Event<Project>> GetEvents(long aggregateId, long? firstSequenceId = null, long? lastEventSequenceNumber = null)
        {
            if (aggregateId < -1)
                return null;

            var query = _context.ProjectEvents.Where(x => x.AggregateId == aggregateId);
            if (firstSequenceId != null && firstSequenceId > 0)
            {
                query = query.Where(x => x.Id >= firstSequenceId);
            }
            if (lastEventSequenceNumber != null && lastEventSequenceNumber > 0)
            {
                query = query.Where(x => x.Id <= lastEventSequenceNumber);
            }
            return query.ToList();
        }

        public async Task<Event<Project>> RaiseEvent(Event<Project> @event)
        {
            await _context.ProjectEvents.AddAsync(@event);
            await _context.SaveChangesAsync();
            return @event;
        }
    }
}
