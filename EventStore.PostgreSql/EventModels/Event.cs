﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventStore.PostgreSql.EventModels
{
    /// <summary>
    /// The event containing the data object
    /// </summary>
    public class Event<T>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long AggregateId { get; set; }

        public DateTimeOffset Timestamp { get; set; } = DateTime.UtcNow;
        
        [MaxLength(250)]
        public string Name { get; set; }

        [Column(TypeName = "jsonb")]
        public T Data { get; set; }

        /// <summary>
        /// The event containing the data of type T
        /// </summary>
        /// <param name="id">Where the event is positioned in the ledger</param>
        /// <param name="aggregateId"></param>
        /// <param name="timestamp">Time when event was recorded</param>
        /// <param name="name">The name of the event. i.e. ProjectCreated, AnyDomainEntityCreated</param>
        /// <param name="data">The data object</param>
        public Event(long id, long aggregateId, DateTimeOffset timestamp, string name, T data)
        {
            Id = id;
            Timestamp = timestamp;
            Name = name;
            Data = data;
            AggregateId = aggregateId;
        }

        /// <summary>
        /// The event containing the data of type T
        /// </summary>
        /// <param name="aggregateId"></param>
        /// <param name="timestamp">Time when event was recorded</param>
        /// <param name="name">The name of the event. i.e. ProjectCreated, AnyDomainEntityCreated</param>
        /// <param name="data">The data object</param>
        public Event(long aggregateId, DateTimeOffset timestamp, string name, T data)
        {
            Timestamp = timestamp;
            Name = name;
            Data = data;
            AggregateId = aggregateId;
        }

        public Event()
        {

        }
    }
}
