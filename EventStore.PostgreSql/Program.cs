using EventStore.PostgreSql.DataContexts;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace EventStore.PostgreSql
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    var services = scope.ServiceProvider;
                    
                    var domainDbContext = services.GetRequiredService<DomainDbContext>();
                    domainDbContext.Database.Migrate();

                    var eventStoreDbContext = services.GetRequiredService<EventDbContext>();
                    eventStoreDbContext.Database.Migrate();
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                    throw new Exception("Program.Database.Error");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .UseIISIntegration()
                .Build();
    }
}
