﻿using EventStore.PostgreSql.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public interface ICompanyDomainStore
    {
        List<Company> GetCompanies();
        Company GetCompany(long id);

        Task<Company> CreateCompany(Company company);
        Task<Company> UpdateCompany(Company company);
        Task DeleteCompany(long id);
    }
}
