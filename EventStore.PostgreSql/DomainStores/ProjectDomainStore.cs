﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public class ProjectDomainStore : IProjectDomainStore
    {
        private readonly DomainDbContext _context;

        public ProjectDomainStore(DomainDbContext context)
        {
            _context = context;
        }

        public async Task<Project> CreateProject(Project project)
        {
            var found = _context.Projects.FirstOrDefault(x => x.Name == project.Name && x.CompanyId == project.CompanyId) != null;

            if (!found)
            {
                await _context.Projects.AddAsync(project);
                await _context.SaveChangesAsync();
                return project;
            }

            throw new ArgumentException();
        }

        public async Task DeleteProject(long id)
        {
            var existingProject = _context.Projects.FirstOrDefault(x => x.Id == id);
            if (existingProject == null)
                throw new ArgumentException();

            _context.Projects.Remove(existingProject);
            await _context.SaveChangesAsync();
        }

        public List<Project> GetAllProjectsByCompanyId(long companyId)
        {
            if (companyId < 1)
                throw new ArgumentNullException($"{nameof(companyId)}");
            return _context.Projects.Where(x => x.CompanyId == companyId)?.ToList();
        }

        public Project GetProjectById(long id)
        {
            if (id < 1)
                throw new ArgumentNullException($"{nameof(id)}");
            return _context.Projects.FirstOrDefault(x => x.Id == id);
        }

        public async Task<Project> UpdateProject(Project updatedProject)
        {
            var existingProject = _context.Projects.FirstOrDefault(x => x.Id == updatedProject.Id);
            if (existingProject != null)
            {
                _context.Entry(existingProject).CurrentValues.SetValues(updatedProject);
                await _context.SaveChangesAsync();
                return updatedProject;
            }
            throw new ArgumentException();
        }
    }
}
