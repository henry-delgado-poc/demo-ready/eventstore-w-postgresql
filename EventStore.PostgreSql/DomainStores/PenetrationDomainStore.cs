﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public class PenetrationDomainStore : IPenetrationDomainStore
    {
        private readonly DomainDbContext _context;

        public PenetrationDomainStore(DomainDbContext context)
        {
            _context = context;
        }

        public async Task<Penetration> CreatePenetration(Penetration penetration)
        {
            await _context.Penetrations.AddAsync(penetration);
            await _context.SaveChangesAsync();
            return penetration;
        }

        public async Task DeletePenetration(long id)
        {
            var existingPenetration = _context.Penetrations.FirstOrDefault(x => x.Id == id);
            if (existingPenetration == null)
                throw new ArgumentException();

            _context.Penetrations.Remove(existingPenetration);
            await _context.SaveChangesAsync();
        }

        public List<Penetration> GetAllPenetrationsByProjectId(long projectId)
        {
            if (projectId < 1)
                throw new ArgumentNullException($"{nameof(projectId)}");
            return _context.Penetrations.Where(x => x.ProjectId == projectId)?.ToList();
        }

        public Penetration GetPenetrationById(long id)
        {
            if (id < 1)
                throw new ArgumentNullException($"{nameof(id)}");
            return _context.Penetrations.FirstOrDefault(x => x.Id == id);
        }

        public async Task<Penetration> UpdatePenetration(Penetration updatedPenetration)
        {
            var existingPenetration = _context.Penetrations.FirstOrDefault(x => x.Id == updatedPenetration.Id);
            if (existingPenetration != null)
            {
                _context.Entry(existingPenetration).CurrentValues.SetValues(updatedPenetration);
                await _context.SaveChangesAsync();
                return updatedPenetration;
            }
            throw new ArgumentException();
        }
    }
}
