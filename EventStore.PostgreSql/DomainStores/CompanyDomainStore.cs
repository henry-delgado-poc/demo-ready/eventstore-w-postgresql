﻿using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public class CompanyDomainStore : ICompanyDomainStore
    {
        private readonly DomainDbContext _context;

        public CompanyDomainStore(DomainDbContext context)
        {
            _context = context;
        }

        public Company GetCompany(long id)
        {
            if(id < 1)
                throw new ArgumentNullException($"{nameof(id)}");
            return _context.Companies.FirstOrDefault(x => x.Id == id);
        }

        public List<Company> GetCompanies()
        {
            return _context.Companies?.ToList();
            
        }

        public async Task<Company> CreateCompany(Company company)
        {
            var found = _context.Companies.FirstOrDefault(x => x.Name == company.Name) != null;

            if(!found)
            {
                await _context.Companies.AddAsync(company);
                await _context.SaveChangesAsync();
                return company;
            }

            throw new ArgumentException();
        }

        public async Task<Company> UpdateCompany(Company updatedCompany)
        {
            var existingCompany = _context.Companies.FirstOrDefault(x => x.Id == updatedCompany.Id);
            if(existingCompany != null)
            {
                _context.Entry(existingCompany).CurrentValues.SetValues(updatedCompany);
                await _context.SaveChangesAsync();
                return updatedCompany;
            }
            throw new ArgumentException();            
        }

        public async Task DeleteCompany(long id)
        {
            var existingCompany = _context.Companies.FirstOrDefault(x => x.Id == id);
            if (existingCompany == null)
                throw new ArgumentException();

            _context.Companies.Remove(existingCompany);
            await _context.SaveChangesAsync();
        }
    }
}
