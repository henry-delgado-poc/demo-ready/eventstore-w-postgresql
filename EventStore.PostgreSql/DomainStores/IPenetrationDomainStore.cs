﻿using EventStore.PostgreSql.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public interface IPenetrationDomainStore
    {
        List<Penetration> GetAllPenetrationsByProjectId(long projectId);
        Penetration GetPenetrationById(long id);

        Task<Penetration> CreatePenetration(Penetration penetration);
        Task<Penetration> UpdatePenetration(Penetration penetration);
        Task DeletePenetration(long id);
    }
}
