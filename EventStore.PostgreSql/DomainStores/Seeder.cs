﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EventStore.PostgreSql.DataContexts;
using EventStore.PostgreSql.DomainModels;
using EventStore.PostgreSql.EventStores;

namespace EventStore.PostgreSql.DomainStores
{
    public class Seeder: ISeeder
    {
        private readonly DomainDbContext _domainDbContext;
        private readonly EventDbContext _eventDbContext;
        private readonly ICompanyDomainStore _companyStore;
        private readonly IProjectDomainStore _projectDomainStore;
        private readonly IPenetrationDomainStore _penetrationDomainStore;
        private readonly IEventStore<Company> _companyEventStore;
        private readonly IEventStore<Project> _projectEventStore;
        private readonly IEventStore<Penetration> _penetrationEventStore;

        public Seeder(DomainDbContext domainDbContext, EventDbContext eventDbContext, ICompanyDomainStore companyDomainStore, IProjectDomainStore projectDomainStore, IPenetrationDomainStore penetrationDomainStore, IEventStore<Company> companyEventStore, IEventStore<Project> projectEventStore, IEventStore<Penetration> penetrationEventStore)
        {
            _domainDbContext = domainDbContext;
            _eventDbContext = eventDbContext;
            _companyStore = companyDomainStore;
            _projectDomainStore = projectDomainStore;
            _penetrationDomainStore = penetrationDomainStore;
            _companyEventStore = companyEventStore;
            _projectEventStore = projectEventStore;
            _penetrationEventStore = penetrationEventStore;
        }

        public async Task GenerateCleanDemoData(int totalCompanies = 2, int totalProjectsPerCompany = 10, int totalPenetrationsPerProject = 10, int totalUpdatesPerRecord = 4)
        {
            //delete all events
            _eventDbContext.PenetrationEvents.RemoveRange(_eventDbContext.PenetrationEvents.ToList());
            _eventDbContext.ProjectEvents.RemoveRange(_eventDbContext.ProjectEvents.ToList());
            _eventDbContext.CompanyEvents.RemoveRange(_eventDbContext.CompanyEvents.ToList());

            //delete all domain models
            _domainDbContext.Penetrations.RemoveRange(_domainDbContext.Penetrations.ToList());
            _domainDbContext.Projects.RemoveRange(_domainDbContext.Projects.ToList());
            _domainDbContext.Companies.RemoveRange(_domainDbContext.Companies.ToList());

            await _eventDbContext.SaveChangesAsync();
            await _domainDbContext.SaveChangesAsync();

            await AddCompanies(totalCompanies, totalProjectsPerCompany, totalPenetrationsPerProject, totalUpdatesPerRecord);


        }

        private async Task AddCompanies(int totalCompanies, int totalProjects, int totalPenetrations, int totalUpdates)
        {
            for (int companyCounter = 1; companyCounter <= totalCompanies; companyCounter++)
            {
                var newCompany = await _companyStore.CreateCompany(new DomainModels.Company("Company " + companyCounter));
                await _companyEventStore.RaiseEvent(new EventModels.Event<Company>(newCompany.Id, DateTime.UtcNow, "addedCompany", newCompany));

                //update the company that you just created. Apply any given amount of updates.4 is default.
                await UpdateCompany(newCompany, totalUpdates);

                await AddProjects(totalProjects, newCompany, totalPenetrations, totalUpdates);
            }
        }

        private async Task UpdateCompany(Company companyToUpdate, int totalUpdates)
        {
            for (int i = 1; i <= totalUpdates; i++)
            {
                companyToUpdate.Name += $" update [{i}]";
                await _companyStore.UpdateCompany(companyToUpdate);
                await _companyEventStore.RaiseEvent(new EventModels.Event<Company>(companyToUpdate.Id, DateTime.UtcNow, "updatedCompany", companyToUpdate));
            }
        }

        private async Task AddProjects(int totalProjects, Company newCompany, int totalPenetrations, int totalUpdates)
        {
            for (int projectCounter = 1; projectCounter <= totalProjects; projectCounter++)
            {
                var newProject = new Project(newCompany.Id, "Project " + projectCounter, "Building " + projectCounter, "Henry");
                await _projectDomainStore.CreateProject(newProject);
                await _projectEventStore.RaiseEvent(new EventModels.Event<DomainModels.Project>(newProject.Id, DateTime.UtcNow, "addedProject", newProject));
                await UpdateProject(newProject, totalUpdates);

                await AddPenetrations(totalPenetrations, newProject, totalUpdates);
            }
        }

        private async Task UpdateProject(Project projectToUpdate, int totalUpdates)
        {
            for (int i = 1; i <= totalUpdates; i++)
            {
                projectToUpdate.Name += $" update [{i}]";
                await _projectDomainStore.UpdateProject(projectToUpdate);
                await _projectEventStore.RaiseEvent(new EventModels.Event<Project>(projectToUpdate.Id, DateTime.UtcNow, "updatedProject", projectToUpdate));
            }
        }

        private async Task AddPenetrations(int totalPenetrations, Project newProject, int totalUpdates)
        {
            for (int penetrationCounter = 1; penetrationCounter <= totalPenetrations; penetrationCounter++)
            {
                var newPenetration = new Penetration(newProject.Id, (10 + penetrationCounter), (1 + penetrationCounter), (10 + penetrationCounter).ToString(), "John", "Created from Seeder", DateTime.UtcNow);
                await _penetrationDomainStore.CreatePenetration(newPenetration);
                await _penetrationEventStore.RaiseEvent(new EventModels.Event<DomainModels.Penetration>(newPenetration.Id, DateTime.UtcNow, "addedPenetration", newPenetration));
                await UpdatePenetration(newPenetration, totalUpdates);
            }
        }

        private async Task UpdatePenetration(Penetration penetrationToUpdate, int totalUpdates)
        {
            for (int i = 1; i <= totalUpdates; i++)
            {
                penetrationToUpdate.Comments = $"Comment update [{i}]";
                await _penetrationDomainStore.UpdatePenetration(penetrationToUpdate);
                await _penetrationEventStore.RaiseEvent(new EventModels.Event<Penetration>(penetrationToUpdate.Id, DateTime.UtcNow, "updatedPenetration", penetrationToUpdate));
            }
        }
    }
}
