﻿using EventStore.PostgreSql.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public interface IProjectDomainStore
    {
        List<Project> GetAllProjectsByCompanyId(long companyId);
        Project GetProjectById(long id);
        Task<Project> CreateProject(Project project);
        Task<Project> UpdateProject(Project project);
        Task DeleteProject(long id);
    }
}
