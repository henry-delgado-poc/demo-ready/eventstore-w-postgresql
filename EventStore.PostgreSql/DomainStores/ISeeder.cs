﻿using System.Threading.Tasks;

namespace EventStore.PostgreSql.DomainStores
{
    public interface ISeeder
    {
        Task GenerateCleanDemoData(int totalCompanies = 2, int totalProjectsPerCompany = 10, int totalPenetrationsPerProject = 10, int totalUpdatesPerRecord = 4);
    }
}
