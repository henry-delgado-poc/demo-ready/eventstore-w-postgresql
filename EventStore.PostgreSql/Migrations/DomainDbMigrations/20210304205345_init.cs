﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EventStore.PostgreSql.Migrations.DomainDbMigrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "demo");

            migrationBuilder.CreateTable(
                name: "Companies",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false),
                    Building = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: true),
                    SubContractorName = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: true),
                    CompanyId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "demo",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Penetrations",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProjectId = table.Column<long>(type: "bigint", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    UlSystem = table.Column<long>(type: "bigint", nullable: false),
                    Floor = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Installer = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: true),
                    Comments = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Penetrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Penetrations_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalSchema: "demo",
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PenetrationImage",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IsPreInstall = table.Column<bool>(type: "boolean", nullable: false),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    PenetrationId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PenetrationImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PenetrationImage_Penetrations_PenetrationId",
                        column: x => x.PenetrationId,
                        principalSchema: "demo",
                        principalTable: "Penetrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Companies_Name",
                schema: "demo",
                table: "Companies",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_PenetrationImage_PenetrationId",
                schema: "demo",
                table: "PenetrationImage",
                column: "PenetrationId");

            migrationBuilder.CreateIndex(
                name: "IX_Penetrations_ProjectId",
                schema: "demo",
                table: "Penetrations",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CompanyId",
                schema: "demo",
                table: "Projects",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_Name",
                schema: "demo",
                table: "Projects",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PenetrationImage",
                schema: "demo");

            migrationBuilder.DropTable(
                name: "Penetrations",
                schema: "demo");

            migrationBuilder.DropTable(
                name: "Projects",
                schema: "demo");

            migrationBuilder.DropTable(
                name: "Companies",
                schema: "demo");
        }
    }
}
