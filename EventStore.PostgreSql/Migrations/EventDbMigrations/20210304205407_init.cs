﻿using System;
using EventStore.PostgreSql.DomainModels;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EventStore.PostgreSql.Migrations.EventDbMigrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "demo");

            migrationBuilder.CreateTable(
                name: "CompanyEvents",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AggregateId = table.Column<long>(type: "bigint", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Name = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Data = table.Column<Company>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyEvents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PenetrationEvents",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AggregateId = table.Column<long>(type: "bigint", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Name = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Data = table.Column<Penetration>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PenetrationEvents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProjectEvents",
                schema: "demo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AggregateId = table.Column<long>(type: "bigint", nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Name = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Data = table.Column<Project>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectEvents", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyEvents_AggregateId",
                schema: "demo",
                table: "CompanyEvents",
                column: "AggregateId");

            migrationBuilder.CreateIndex(
                name: "IX_PenetrationEvents_AggregateId",
                schema: "demo",
                table: "PenetrationEvents",
                column: "AggregateId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectEvents_AggregateId",
                schema: "demo",
                table: "ProjectEvents",
                column: "AggregateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyEvents",
                schema: "demo");

            migrationBuilder.DropTable(
                name: "PenetrationEvents",
                schema: "demo");

            migrationBuilder.DropTable(
                name: "ProjectEvents",
                schema: "demo");
        }
    }
}
