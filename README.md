# Event Store with PostgreSQL

## Requisite
You need to have Docker Compose. Install it if needed
```
sudo apt install docker-compose
```

Run... to start
```
docker-compose up
```

**Sites**:
* [API](https://localhost:49195/swagger/index.html)
* [Adminer | UI to manage SQL](http://localhost:8081/)
* [pgAdmin4 | UI to manage SQL](http://localhost:8081/)

Run... to stop services
```
docker-compose down
```